function parse_coordinates(url, callback) {
	//debugger;
	$.getJSON(url, function (data) {
			var features = data["features"]
			var coordinates = []
			//debugger

			for (var x in features) {
				var latlong = features[x]["geometry"]["coordinates"];
				//debugger;
				coordinates.push([latlong[0], latlong[1]])
				
			};

			callback(coordinates)
		}
	);	
}

function plot_points(coordinates) {
	// include GeoJSON inside this JavaScript
	//debugger;
	var haiti = {"type":"FeatureCollection","features":[
	{"type":"Feature","properties":{"name":"Haiti"},"geometry":{"type":"Polygon","coordinates":[coordinates]},"id":"HTI"}
	]};

	L.geoJson( haiti, {
	    style: function (feature) {
	        return { opacity: 0, fillOpacity: 0.5, fillColor: "#0f0" };
	    },
	    onEachFeature: function(feature, layer){
	        layer.bindPopup("Hello GeoJSON!");
	    }
	}).addTo(map);

	//debugger;
	//debugger;
	//map.addLayers(markers);

	/*var heat = L.heatLayer(quakePoints,{
            radius: 20,
            blur: 15, 
            maxZoom: 17,
        }).addTo(map);*/
}

function initialize_map() {
	// create a map
	map = new L.Map('mymap');

	// create the OpenStreetMap layer
	var osmTile = "http://tile.openstreetmap.org/{z}/{x}/{y}.png";
	var osmCopyright = "Map data &copy; 2012 OpenStreetMap contributors";
	var osmLayer = new L.TileLayer(osmTile, { maxZoom: 18, attribution: osmCopyright } );
	
	map.addLayer(osmLayer);

	// set the map's starting view
	map.setView( new L.LatLng(20, -73), 7 );
	return map
}

function main() {
	//debugger;
	var map = initialize_map()
	
	var urllist = ["js/data.json"]
	
	var time = 0, next = 15;

	for (var url in urllist) {
		
		setTimeout(function(){
			parse_coordinates(urllist[0], function(coordinates){
				plot_points(coordinates)
			})
		}, time)
		
		time += 2;
	}
}

$(function(){
	console.log("in main.js")
	main()
})